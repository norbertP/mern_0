﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const db = require('_helpers/db');
const { ObjectId } = require('mongodb');
const User = db.User;



//Les posts sont stockés dans la collection users

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll(req) {

    //Attention, cas sur un seul user.
    const userId = req.user.sub;
    const users = await User.find({_id:userId})
    const posts = users[0].posts;
    return posts
}

async function getById(id) {
    return await User.findById(id);
}

async function create(userId, post) {
    const user = await User.findById(userId);
    if (user) {
        if (!user.posts)
            user.posts = [];

        console.log("post",post);
        const postId = mongoose.Types.ObjectId();

        post.id = postId;
        post.creationDate = new Date().toISOString();
        user.posts.push(post)

        return await user.save()
            .then(user => user)
            .catch(error => error)
    }
    return "User not found"
}

async function update(id, userParam) {

    const userID = req.user.sub ;
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(userId,id) {

    const user = await User.findById(userId);
    if (user) {
        if (!user.posts)
            user.posts = [];

        if (user.posts && user.posts) {
            var newObject = user.posts.filter(post => post.id.toString() !== id)

            user.posts = newObject;
        }

        return await user.save()
            .then(() => user.posts)
            .catch(error => error)
    }
    return "User not found"
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}