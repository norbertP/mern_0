﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const { ObjectId } = require('mongodb');
const User = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    followUser,
    unFollowUser
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const token = jwt.sign({ sub: user.id }, config.secret);
        return {
            ...user.toJSON(),
            token
        };
    }
}

async function getAll(req) {
    let requestFilter = req.query.select || {};
    let str;
    const strkey =  "followin." + req.user.sub;

    switch (requestFilter)
    {
        case 'followedByConnectUser':
            //str = `"followin.${req.user.sub}": {$exists: 1}`;

            str = {[strkey] : {$exists: 1}};
        break;
        case 'notFollowedByConnectUser':
            str = {[strkey] : {$exists: 0}};
        break;
        default:
            str=undefined;
    }
    const filter = str ? str:{};
    console.log(filter);
    return await User.find(filter);
}

async function getById(id) {
    return await User.findById(id);
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function followUser(id, idToFollow) {
    await timeout(800);
    
    const user = await User.findById(id);
    if (user) {
        if (!user.followin)
            user.followin = {};

        const userFollowed = await User.findById(idToFollow);
        if (userFollowed)
            user.followin = { [idToFollow]: userFollowed, ...user.followin };

        return await user.save()
            .then(user => user)
            .catch(error => error)
    }
    return "User not found"

}

async function unFollowUser(id, idToUnFollow) {
    await timeout(500);
    const user = await User.findById(id);
    if (user) {
        if (user.followin && user.followin[idToUnFollow]) {
            const newObject = { ...user.followin };
            delete newObject[idToUnFollow]

            user.followin = newObject;
        }
        return await user.save()
            .then(user => user)
            .catch(error => error)
    }
    return "User not found"
}