﻿const express = require('express');
const router = express.Router();
const userService = require('./user.service');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);
router.post('/current/follow/:idToFollow', followUser);
router.delete('/current/follow/:idToUnFollow', unFollowUser);

module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll(req)
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

async function followUser(req, res, next) {

    //l'utilisateur "followin" est l'utilisateur connecté via le auth de jwt, l'info se trouve dans req.user.sub
    await userService.followUser(req.user.sub, req.params.idToFollow)
        .then(user => res.json(user))
        .catch(err => next(err));


}

function unFollowUser(req, res, next) {
    //l'utilisateur "followin" est l'utilisateur connecté via le auth de jwt, l'info se trouve dans req.user.sub
    userService.unFollowUser(req.user.sub, req.params.idToUnFollow)
        .then(user => res.json(user))
        .catch(err => next(err));
}

