import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute, NavBar } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../AuthPages/LoginPage';
import { RegisterPage } from '../AuthPages/RegisterPage';
import { UpdatePage } from '../AuthPages/UpdatePage';

class App extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <div className="container">
                <Router history={history}>
                    <NavBar></NavBar>
                    <Switch>
                        <div className="jumbotrons">
                            <div className="container">


                                {alert.message &&
                                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                                }

                                <PrivateRoute path={["/", "/posts/create", "/posts/update"]} exact component={HomePage} />
                                <div className="col-sm-8 col-sm-offset-2">
                                    <Route path="/login" component={LoginPage} />
                                    <Route path="/register" component={RegisterPage} />
                                    <Route path="/update-account" component={UpdatePage} />
                                    <Redirect from="*" to="/" />
                                </div>

                            </div>
                        </div>
                    </Switch>
                </Router>
            </div>
        );
    }
}

function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };