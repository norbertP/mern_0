import React from 'react';
import { Link , Route, Router, Switch} from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { userActions } from '../_actions';
import { MemberList} from '../_components/members';
import { PostList, PostItem } from '../_components/posts';
import { PrivateRoute } from '../_components';

class HomePage extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-4">
                    <MemberList title="Available members to follow"/>
                </div>
                <router>
                <div className="col-md-8">
                <Router history={history}>
                    <Route path="/posts/create" component={() => <PostItem mode="create" title={"Add a post"}/>} />
                </Router>
                <Router history={history}></Router>

                <PostList title="post of current user" />
            </div>
            </router>
            </div>
        )
    }

    render2() {
        const { user, users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h1>Hi {user.firstName}!</h1>
                <p>You're logged in with React!!</p>
                <h3>All registered users:</h3>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <ul>
                        {users.items.map((user, index) =>
                            <li key={user.id}>
                                {user.firstName + ' ' + user.lastName}
                                {
                                    user.deleting ? <em> - Deleting...</em>
                                        : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                            : <span> - <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>
                                }
                            </li>
                        )}
                    </ul>
                }
                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {

}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };