import { memberConstants } from '../_constants';
import { memberService } from '../_services';
import { alertActions } from '.';
import { history } from '../_helpers';

export const memberActions = {
    getAll,
    followMember,
    unFollowMember
};

function getAll(param) {
    console.log('CAll API to get ALL', param);
    return dispatch => {
        dispatch(request());

        memberService.getAll(param)
            .then(
                members => dispatch(success(members)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: memberConstants.GETALL_REQUEST } }
    function success(members) { return { type: memberConstants.GETALL_SUCCESS, members} }
    function failure(error) { return { type: memberConstants.GETALL_FAILURE, error } }
}

function followMember(userToFollow) {
    console.log("Dans followMember",userToFollow)
    return dispatch => {
        dispatch(request(userToFollow));

        memberService.followMember(userToFollow)
            .then(
                //Attention, c'est le refresh du membre que l'on suivi qu'il faut mettre à jour
                user => {dispatch(success(user, userToFollow))},
                error => dispatch(failure(userToFollow, error.toString()))
            );
    };

    function request(member) { return { type: memberConstants.FOLLOW_REQUEST, member } }
    function success(user, member) { return { type: memberConstants.FOLLOW_SUCCESS, user, member } }
    function failure(member, error) { return { type: memberConstants.FOLLOW_FAILURE, member, error } }
}

function unFollowMember(userToUnFollow) {
    return dispatch => {
        dispatch(request(userToUnFollow));

        memberService.unFollowMember(userToUnFollow)
            .then(
                user => dispatch(success(user, userToUnFollow)),
                error => dispatch(failure(userToUnFollow, error.toString()))
            );
    };

    function request(member) { return { type: memberConstants.UNFOLLOW_REQUEST, member } }
    function success(user, member) { return { type: memberConstants.UNFOLLOW_SUCCESS, user, member } }
    function failure(member, error) { return { type: memberConstants.UNFOLLOW_FAILURE, member,error } }
}