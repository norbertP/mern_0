import { postConstants } from '../_constants';
import { postService } from '../_services';
import { alertActions } from '.';
import { history } from '../_helpers';

export const postActions = {
    add,
    //getOne,
    getAll,
    delete: _delete
    //update
};



function add(post) {
    console.log("post : ", post);
    return dispatch => {
        dispatch(request(post));

        postService.add(post)
            .then(
                post => { 
                    dispatch(success(post));
                    history.push('/');
                    dispatch(alertActions.success('Post created successfully'));
                    dispatch(getAll());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(post) { return { type: postConstants.CREATE_REQUEST, post } }
    function success(post) { return { type: postConstants.CREATE_SUCCESS, post } }
    function failure(error) { return { type: postConstants.CREATE_FAILURE, error } }
}

function update(post) {
    return dispatch => {
        dispatch(request(post));

        postService.update(post)
            .then(
                post => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Updating successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(post) { return { type: postConstants.UPDATE_REQUEST, post } }
    function success(post) { return { type: postConstants.UPDATE_SUCCESS, post } }
    function failure(error) { return { type: postConstants.UPDATE_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        postService.getAll()
            .then(
                posts => dispatch(success(posts)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: postConstants.GETALL_REQUEST } }
    function success(posts) { return { type: postConstants.GETALL_SUCCESS, posts } }
    function failure(error) { return { type: postConstants.GETALL_FAILURE, error } }
}



// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        postService.delete(id)
            .then(
                posts => dispatch(success(posts)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: postConstants.DELETE_REQUEST, id } }
    function success(posts) { return { type: postConstants.DELETE_SUCCESS, posts } }
    function failure(id, error) { return { type: postConstants.DELETE_FAILURE, id, error } }
}