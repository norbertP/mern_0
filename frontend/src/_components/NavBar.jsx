import React from 'react';
import {
  Link
} from "react-router-dom";import { connect } from 'react-redux';
import { alertActions } from '../_actions';


class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loggedIn : false,
        user: "Not logged"
    };

}

  render() {

    const { user, loggedIn } = this.props;
    console.log("ici", this.props);

    return (
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-brand" href="#">WebSiteName</a>
          </div>
          <ul className="nav navbar-nav">
            <li className="active"><a href="#">Home</a></li>
            {loggedIn && <li><Link to="/posts/create">Create a post</Link></li> }
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li><a href="#">connected as {user && user.firstName}</a></li>
            {loggedIn && <li><Link to="/login"><span className="glyphicon glyphicon-off"></span></Link></li> }
            {loggedIn && <li><Link to="/update-account">Update account</Link></li> }
            {!loggedIn && <li><Link to="/register"><span className="glyphicon glyphicon-pencil"></span></Link></li> }
            {!loggedIn && <li><Link to="/login"><span className="glyphicon glyphicon-user"></span></Link></li> }
          </ul>
        </div>
      </nav>
    )
  }
}

function mapState(state) {
  const { authentication } = state;
  const { user , loggedIn} = authentication;
  return { user, loggedIn };
}

const actionCreators = {
  clearAlerts: alertActions.clear
}

const handleAuthNavBar = connect(mapState, actionCreators)(NavBar);
export { handleAuthNavBar as NavBar};