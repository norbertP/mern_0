import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { memberActions } from '../../_actions';
import { ActionButton } from '../tools';

class MemberItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            member: this.props.member
        }

        this.handleToggleFollow = this.handleToggleFollow.bind(this);
    }

    handleToggleFollow() {
        const followin = this.props.connectedMember.followin;
        const follow = followin && followin[this.props.member.id]
        const follow_reverse = !follow
        if(follow_reverse)
            this.props.followMember(this.state.member)
        else
            this.props.unFollowMember(this.state.member)

    }

    render() {
        const { member } = this.props;
        const { toggling } = member;
       
        const followin = this.props.connectedMember.followin;
        const follow = followin && followin[member.id]

        return (<React.Fragment>
            <td scope="row">{member.firstName}</td>
            <td>{member.lastName}</td>
            <td align="center" width="110px">
                <ActionButton pending={toggling} className={follow ? "btn btn-success btn-block" : "btn btn-info btn-block"} onClick={this.handleToggleFollow}>
                    {follow ? "Following" : "Follow"}
                </ActionButton>
            </td>
        </React.Fragment>
        )
    }
}

function mapState(state) {
    //Ici, on a accès à tout le state global !!
    console.log("Ici, on a accès à tout le state global", state)
    const { members2 } = state ;
    const { connectedMember } = members2 ;
    return { connectedMember };
}

const actionCreators = {
    followMember: memberActions.followMember,
    unFollowMember: memberActions.unFollowMember
}

const connectedMemberItem = connect(mapState, actionCreators)(MemberItem);
export { connectedMemberItem as MemberItem };