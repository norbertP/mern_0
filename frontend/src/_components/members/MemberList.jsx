import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { memberActions } from '../../_actions';
const {MemberItem} =require('./MemberItem');
import { getVisibleMembers } from '../../_selectors';
import {memberConstants} from '../../_constants/member.constants'


class MemberList extends React.Component {

    getFilterType()
    {
        const filterType = this.props.filter && 
                        this.props.filter.select &&
                        this.props.filter.select === 'followedByConnectUser'
                        ? memberConstants.SHOW_MEMBERS_ALL :
                        memberConstants.SHOW_MEMBERS_FOLLOWIN
        return filterType
    }

    componentDidMount() {
        this.props.getMembers({ ...this.props.filter});
    }

    handleDeleteUser(id) {
        return (e) => this.props.deleteUser(id);
    }

    render() {
        const { members, title } = this.props;

        const { items } = members ;

        return (
            <React.Fragment>
                <p>{title}</p>
                {items &&
                    <table className="table table-striped">
                        <tbody>
                            {
                                items.map(member => <tr key={member.id}><MemberItem member={member}/></tr>)
                            }
                        </tbody>
                    </table>
                }
            </React.Fragment>
        )

    }
}

function mapState(state) {

const members = getVisibleMembers(state,memberConstants.SHOW_MEMBERS_ALL);
    return { members };
}

const actionCreators = {
    getMembers: memberActions.getAll
}

const connectedMemberList = connect(mapState, actionCreators)(MemberList);
export { connectedMemberList as MemberList };