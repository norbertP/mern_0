import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { postActions } from '../../_actions';
import { ActionButton } from '../tools';

class PostItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };

        this.handleChange = this.handleChange.bind(this);
        this.handlePosting = this.handlePosting.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(e) {
        e.preventDefault();
        this.props.delete(this.props.post.id);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }


    handlePosting(e) {
        e.preventDefault();
        const content = this.state.content;
        console.log("Content : ", content);
        this.props.add({ post: { content } });
    }

    render() {
        const addPosting = this.props.addPosting;
        let render;
        const { title } = this.props
        const post = this.props.post;
        const lenContent = 140;//- this.state.content.length;
        if (this.props.mode === "create" || this.props.mode === "update") {
            render = <React.Fragment>
                <div className="form-group">
                    <label for="exampleFormControlTextarea1">{title} - remains : {lenContent} char{lenContent > 1 ? 's' : ''}</label>
                    <textarea maxlength={140} className="form-control" id="content" name="content" rows="3" onChange={this.handleChange}>
                        {post ? post.content : ''}
                    </textarea>
                </div>
                <ActionButton pending={addPosting} className="btn btn-primary btn-block" onClick={this.handlePosting}>
                    Post it!
        </ActionButton>
            </React.Fragment>
        }
        else {
            const createdDate = new Date(post.creationDate)
            render = <tr>
                <td width="30">
                    <span >
                    <a className="glyphicon glyphicon-trash small" onClick={this.handleDelete}></a>
                    </span>
                </td>

                <td width="150" >
                    {createdDate.toLocaleDateString('fr-FR') + " " + createdDate.toLocaleTimeString('fr-FR')}
                </td>
                <td >
                    {post.content}
                </td>
            </tr>
        }
        return render
    }
}


function mapState(state) {
    const { addPosting } = state;
    return { addPosting };
}

const actionCreators = {
    add: postActions.add,
    get: postActions.getById,
    delete : postActions.delete
}

const connectedPostItem = connect(mapState, actionCreators)(PostItem);
export { connectedPostItem as PostItem };
