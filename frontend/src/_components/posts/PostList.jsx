import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { postActions } from '../../_actions';
const { PostItem } = require('./PostItem');
//import { getVisiblePosts } from '../../_selectors';
import { postConstants } from '../../_constants/post.constants'


class PostList extends React.Component {

    componentDidMount() {
        this.props.getPosts();
    }

    handleDelete(id) {
        return (e) => this.props.delete(id);
    }

    render() {
        const { connectedMemberPosts, title } = this.props;
        const items = connectedMemberPosts;
        console.log("posts dans render", connectedMemberPosts);
        return (
            <React.Fragment>
                <p>{title}</p>
                {items &&
                    <table className="table table-striped">
                        <tbody>
                            {
                                items.map(post => <tr key={post.id}><PostItem post={post} mode="view" /></tr>)
                            }
                        </tbody>
                    </table>
                }
            </React.Fragment>
        )

    }
}

function mapState(state) {
    const { posts } = state;
    const { connectedMemberPosts } = posts;
    return { connectedMemberPosts };
}

const actionCreators = {
    getPosts: postActions.getAll
}

const connectedPostList = connect(mapState, actionCreators)(PostList);
export { connectedPostList as PostList };