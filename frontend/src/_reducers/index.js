import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { members2 } from './members.reducer';
import { alert } from './alert.reducer';
import { posts } from './posts.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  members2,
  users,
  alert,
  posts
});

export default rootReducer;