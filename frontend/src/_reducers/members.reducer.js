import { memberConstants, userConstants } from '../_constants';

//Le nom de cette fonction sera aussi le nom de l'object à la racine du state globale
//ou seront stocké toutes les données manipuler dans la fonction !!
//On doit retourner l'ensemble des données à jour !

//CEtte partie sert lorsqu'on fait par exemple F5
let connectedMember = JSON.parse(localStorage.getItem('user'));
const initialState = connectedMember ? { connectedMember } : {};


function getItemKey(action) { 
  const key = "items"; 
  return key ;
};

export function members2(state = initialState, action) {
  const itemsKey = getItemKey(action);

  switch (action.type) {

    case userConstants.LOGIN_SUCCESS:
      return {
        ...state,
        connectedMember: action.user
      };

    case memberConstants.GETALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case memberConstants.GETALL_SUCCESS:
      return {
        ...state,
        [itemsKey]: action.members
      };
    case memberConstants.GETALL_FAILURE:
      return { 
        ...state,
        error: action.error
      };
    case memberConstants.FOLLOW_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        [itemsKey]: state[itemsKey].map(member =>
          member.id === action.member.id
            ? { ...member, toggling: true }
            : member
        )
      };
    case memberConstants.FOLLOW_SUCCESS:
      return {
        ...state,
        connectedMember : action.user,
        [itemsKey]: state[itemsKey].map(member =>
          member.id === action.member.id
            ? { ...member, toggling: false }
            : member
        )
      };
    case memberConstants.FOLLOW_FAILURE:
     return {
        ...state,
        [itemsKey]: state[itemsKey].map(member => {
          if (member.id === action.member.id) {
            // make copy of member without 'toggling:true' property
            const { toggling, ...memberCopy } = member;
            // return copy of member with 'followError:[error]' property
            return { ...memberCopy, followError: action.error };
          }

          return member;
        })
      };


      case memberConstants.UNFOLLOW_REQUEST:

        return {
          ...state,
          [itemsKey]: state[itemsKey].map(member =>
            member.id === action.member.id
              ? { ...member, toggling: true }
              : member
          )
        };
      case memberConstants.UNFOLLOW_SUCCESS:
        return {
          ...state,
          connectedMember : action.user,
          [itemsKey]: state[itemsKey].map(member =>
            member.id === action.member.id
              ? { ...member, toggling: false }
              : member
          )
        };
      case memberConstants.UNFOLLOW_FAILURE:
        return {
          ...state,
          [itemsKey]: state[itemsKey].map(member => {
            if (member.id === action.member.id) {
              // make copy of member without 'toggling:true' property
              const { toggling, ...memberCopy } = member;
              // return copy of member with 'followError:[error]' property
              return { ...memberCopy, followError: action.error };
            }
  
            return member;
          })
        };

      case memberConstants.UPDATE_REQUEST:
      // add 'updating:true' property to user being updating
      return {
        ...state,
        [itemsKey]: state[itemsKey].map(user =>
          user.id === action.id
            ? { ...user, toggling: true }
            : user
        )
      };
    case memberConstants.UPDATE_SUCCESS:
      // remove updating user from state
      return {
        //items: state.items.filter(user => user.id !== action.id),
        ...state,
        updatedUserId : action.id
      };
    case memberConstants.UPDATE_FAILURE:
      // remove 'updating:true' property and add 'updatingError:[error]' property to user 
      return {
        ...state,
        [itemsKey]: state[itemsKey].map(user => {
          if (user.id === action.id) {
            // make copy of user without 'updating:true' property
            const { toggling, ...userCopy } = user;
            // return copy of user with 'updatingError:[error]' propertys
            return { ...userCopy, updatingError: action.error };
          }

          return user;
        })
      };

    default:
      return state
  }
}