import { postConstants } from '../_constants';

export function posts(state = {posts:[]}, action) {
  switch (action.type) {

    case postConstants.CREATE_REQUEST:
      // add 'updating:true' property to post being updating
      return {
        ...state, addPosting: true
      };
    case postConstants.CREATE_SUCCESS:
      return {
        ...state, 
        addPosting: false
      };
    case postConstants.CREATE_FAILURE:
      const { addPosting, ...stateCopy } = state;
      return  {...stateCopy, addingError: action.error};

    case postConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case postConstants.GETALL_SUCCESS:
      const { loading, ...stateCopy2 } = state;
      return {
        connectedMemberPosts : action.posts
      };
    case postConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
    case postConstants.DELETE_REQUEST:
      // add 'deleting:true' property to post being deleted
      return {
        deleting: true
      };
    case postConstants.DELETE_SUCCESS:
      // remove deleted post from state
      return {
        connectedMemberPosts: action.posts
      };
    case postConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to post 
      return {
        ...state,
        items: state.items.map(post => {
          if (post.id === action.id) {
            // make copy of post without 'deleting:true' property
            const { deleting, ...postCopy } = post;
            // return copy of post with 'deleteError:[error]' property
            return { ...postCopy, deleteError: action.error };
          }

          return post;
        })
      };

      case postConstants.UPDATE_REQUEST:
      // add 'updating:true' property to post being updating
      return {
        ...state,
        items: state.items.map(post =>
          post.id === action.id
            ? { ...post, updating: true }
            : post
        )
      };
    case postConstants.UPDATE_SUCCESS:
      // remove updating post from state
      return {
        //items: state.items.filter(post => post.id !== action.id),
        updatedUserId : action.id
      };
    case postConstants.UPDATE_FAILURE:
      // remove 'updating:true' property and add 'updatingError:[error]' property to post 
      return {
        ...state,
        items: state.items.map(post => {
          if (post.id === action.id) {
            // make copy of post without 'updating:true' property
            const { updating, ...postCopy } = post;
            // return copy of post with 'updatingError:[error]' propertys
            return { ...postCopy, updatingError: action.error };
          }

          return post;
        })
      };

    default:
      return state
  }
}