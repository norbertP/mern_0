import { createSelector } from 'reselect'

import {memberConstants} from '../_constants/member.constants'


const getVisible = (state, type) => type
const getMembers = (state) => state.members2
const getConnectedMember = (state) => state.members2.connectedMember


export const getVisibleMembers = createSelector(
	[getVisible, getMembers, getConnectedMember],
	(visibilityFilter, members, connectedMember) => {
		switch (visibilityFilter) {
			case memberConstants.SHOW_MEMBERS_ALL:
				return members
			case memberConstants.SHOW_MEMBERS_FOLLOWIN:
				return members.items.filter((t) => t._id === connectedMember.id)
			case memberConstants.SHOW_MEMBERS_UNFOLLOWIN:
				return members.items.filter((t) => t._id !== connectedMember.id)
			default:
				throw new Error(`Unknown filter:${visibilityFilter}`)
		}
	},
)