import config from 'config';
import { authHeader } from '../_helpers';

export const memberService = {
    getAll,
    followMember,
    unFollowMember
};

function convertObjectToURIParameters(object)
{
    return Object.entries(object)
    .map(
      ([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
    )
    .join("&");
}

function getAll(param) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    let params;
    if(param && param instanceof Object)
        params = '?' + convertObjectToURIParameters(param);
    else    
        params = '';

    return fetch(`${config.apiUrl}/users${params}`, requestOptions).then(handleResponse);
}

function followMember(userToFollow) {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body:{}
    };

    return fetch(`${config.apiUrl}/users/current/follow/${userToFollow.id}`, requestOptions).then(handleResponse);
}

function unFollowMember(userToFollow) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader(),
        body:{}
    };

    return fetch(`${config.apiUrl}/users/current/follow/${userToFollow.id}`, requestOptions).then(handleResponse);
}


function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/members/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}